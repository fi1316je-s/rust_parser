use regex::bytes::{Regex,RegexSet,Captures};
use std::io::{BufRead,BufReader,SeekFrom,Seek,Write};
use std::fs::{File,OpenOptions};
use std::collections::HashMap;
use csv::{Writer,WriterBuilder};
use serde::Serialize;
use std::env;
use std::thread::{self, JoinHandle};
use num_cpus;
#[macro_use] extern crate lazy_static;

static NEW_LINE: u8 = 0x0a;

lazy_static! {
    static ref LP_BEGIN_K: Vec<&'static str> = vec![
        "L1_Srv_Cll_Ms_Rsp",
        "L1_Srv_Cll_Ms_Rslt",
        "L1_PUCCH_Tx_Rpt",
        "L1_PUSCH_Tx_Rpt",
        "PDCP_DL_CD_PDU",
        "PDCP_UL_CD_PDU",
        "PDCCH-PHICH_Ind_Rpt",
        "DCI_Info_Rpt",
        "MAC_UL_TB",
        "MAC_DL_TB",
        "L1_PDSCH_Dc_Rslt",
        "L1_PDCCH_Dc_Rslt",
        "RRC_OTA_Packet",
        "GNSS_Position_Rpt",
        "L1_Nhbr_Cll_Ms_Trk",
        "Nhbr_Ms_Rqst_Rsp"
    ];
    static ref LP_BEGIN_RS: RegexSet = RegexSet::new(&[
        r"0xB193  LTE ML1 Serving Cell Meas Response",
        r"0xB116  LTE LL1 Serving Cell Measurement Results",
        r"0xB13C  LTE LL1 PUCCH Tx Report",
        r"0xB139  LTE LL1 PUSCH Tx Report",
        r"0xB0A3  LTE PDCP DL Cipher Data PDU",
        r"0xB0B3  LTE PDCP UL Cipher Data PDU",
        r"0xB16B  LTE PDCCH-PHICH Indication Report",
        r"0xB16C  LTE DCI Information Report",
        r"0xB064  LTE MAC UL Transport Block",
        r"0xB063  LTE MAC DL Transport Block",
        r"0xB132  LTE LL1 PDSCH Decoding Results",
        r"0xB130  LTE LL1 PDCCH Decoding Result",
        r"0xB0C0  LTE RRC OTA Packet",
        r"0x1476  GNSS Position Report",
        r"0xB119  LTE LL1 Neighbor Cell Measurements and Tracking",
        r"0xB195  LTE ML1 Connected Neighbor Meas Request/Response"
    ]).unwrap();
    static ref LPV_RS_MAP: HashMap<&'static str, Vec<&'static str>> = [
        ("L1_Srv_Cll_Ms_Rsp",vec![
            r"^   E-ARFCN                   = (\d*)",
            r"^   Num of Cell               = (\d*)",
            r"^      Physical Cell ID          = (\d*)", 
            r"^      Serving Cell Index        = (\w*)",
            r"^      Is Serving Cell           = (\d*)",
            r"^      Current SFN               = (\d*)",
            r"^      Current Subframe Number   = (\d*)",
            r"^      Inst Measured RSRP        = (.\d*\.\d*)",
            r"^      Inst RSRQ                 = (.\d*\.\d*)",
            r"^      Inst RSSI                 = (.\d*\.\d*)",
        ]),
        ("L1_Srv_Cll_Ms_Rslt",vec![
            r"^System FN              = (\d*)$",
            r"^Sub-frame Number       = (\d*)$",
            r"^Cell ID                = (\d*)$",
            r"^Timing Offset Rx       = \{\s(\d*),\s*(\d*)",
        ]),
        ("L1_PUCCH_Tx_Rpt",vec![
            r"^Serving Cell ID    = (\d*)",
            r"^   \|\s*(\d*)(\d)\|.{16}\|.{7}\|\s*(\w+)\|.{5}\|.{5}\|.{11}\|.{3}\|.{4}\|.{4}\|.{28}\|.{7}\|\s*(-?\d+)\|.{20}\|\s*(\d+)\|",
        ]),
        ("L1_PUSCH_Tx_Rpt",vec![
            r"^Serving Cell ID    = (\d*)",
            r"^   \|\s*(\d*)(\d)\|.{7}\|.{6}\|.{6}\|.{6}\|.{11}\|\s*(\w+)\|.{6}\|.{7}(.{11})?\|.{5}\|.{5}\|.{3}\|.{7}\|.{7}\|.{7}\|.{7}\|.{7}\|.{7}\|.{3}\|.{8}\|.{20}\|.{7}\|.{7}\|.{12}\|\s*(\w+)\|.{7}(.{18})?\|\s*(-?\d+)\|.{4}\|.{7}\|.{8}\|.{6}\|.{4}\|.{4}\|.{5}\|.{48}\|.{15}\|.{9}\|.{9}\|.{4}\|.{4}\|$",
        ]),
        ("PDCP_DL_CD_PDU",vec![
            r"^.{27}\|\s*(\w+)\s*\|\s*(\d+) bit\s*\|.{12}\|\s*(\d+)\s*\|.{6}\|\s*(\d+)\s*\|\s*(\d)\s*\|.{10}\|\s*(\d+)\s*\|.{29}\|$",
        ]),
        ("PDCP_UL_CD_PDU",vec![
            r"^   \|.{17}\|.{3}\|\s*(\w+)\s*\|\s*(\d+) bit\s*\|.{6}\|.{5}\|\s*(\d+)\s*\|.{6}\|\s*(\d+)\s*\|\s*(\d)\s*\|.{10}\|\s*(\d+)\s*\|.{10}\|.{8}\|.{4}\|.{7}\|.{8}\|.{3}\|.{6}\|.{24}\|$",
        ]),
        ("PDCCH-PHICH_Ind_Rpt",vec![
            r"^   \|.{3}\|.{7}\|.{7}\|\s*(\d+)\|\s{5}(\d)\|(.{8}\|)?.{5}\|\s{5}Yes\|\s*\w+\|\s*(\w*)\|\s*(\w*)\|.{5}\|.{14}\|.{7}\|.{11}\|.{6}\|.{10}\|.{5}\|.{6}\|.{5}\|.{5}\|.{5}\|.{5}\|.{6}\|.{6}\|.{8}\|.{5}\|$",
        ]),
        ("DCI_Info_Rpt",vec![
            r"^   \|.{3}\|\s*(\d+)\|\s*(\d)\|.{5}\|\s*Yes\|.{5}\|.{10}\|.{5}\|.{5}\|.{8}\|\s*(\d+)\|\s*(\d+)\|.{3}\|.{6}\|.{5}\|.{9}\|.{7}\|.{7}\|.{8}\|.{8}\|.{5}\|.{8}\|\s*(.+)\|.{8}\|.{8}\|.{4}\|.{10}\|\s*(\d+)\|.{15}\|.{11}\|.{6}\|.{9}\|.{6}\|\s*(Yes)?\|.{5}\|\s*(\w*)\|.{7}\|.{7}\|.{3}\|.{5}\|.{5}\|.{15}\|.{11}\|.{6}\|.{7}\|.{7}\|.{10}\|.{14}\|.{7}\|$",
            r"^   \|.{3}\|\s*(\d+)\|\s*(\d)\|.{5}\|.{10}\|.{5}\|.{10}\|.{5}\|.{5}\|.{8}\|.{5}\|.{3}\|.{3}\|.{6}\|.{5}\|.{9}\|.{7}\|.{7}\|.{8}\|.{8}\|.{5}\|.{8}\|.{10}\|.{8}\|.{8}\|.{4}\|.{10}\|.{4}\|.{15}\|.{11}\|.{6}\|.{9}\|.{6}\|\s*Yes\|.{5}\|\s*(\w+)\|.{7}\|.{7}\|.{3}\|.{5}\|.{5}\|.{15}\|.{11}\|.{6}\|.{7}\|.{7}\|.{10}\|.{14}\|.{7}\|$",
        ]),
        ("MAC_UL_TB",vec![
            r"^\s{6}\|.{3}\|.{4}\|\s*(\d+)\|.{10}\|\s*(\d)\|\s*(\d+)\|\s*(\d+)\|.{5}\|.{7}\|.{17}\|\s*(.+)\|.{5}\|.{24}\|\s*(.+)?\|.{5}\|.{5}\|.{5}\|.{5}\|.{5}\|\s*(\d+)?\|.{9}\|.{9}\|.{9}\|.{9}\|.{7}\|.{6}\|.{7}\|.{6}\|.{7}\|$",
            r"^\s{6}\|.{3}\|.{4}\|.{7}\|.{10}\|.{6}\|.{5}\|.{7}\|.{5}\|.{7}\|.{17}\|.{15}\|.{5}\|.{24}\|\s*PHR\|.{5}\|.{5}\|.{5}\|.{5}\|.{5}\|\s*(\d+)?\|.{9}\|.{9}\|.{9}\|.{9}\|.{7}\|.{6}\|.{7}\|.{6}\|.{7}\|$",
        ]),
        ("MAC_DL_TB",vec![
            r"^      \|.{3}\|.{4}\|\s*(\d)\|\s*(\d+)\|.{10}\|\s*(\d+)\|.{4}\|.{4}\|\s*(\d+)\|.{4}\|.{5}\|.{7}\|.{5}\|.{24}\|.{15}\|.{5}\|.{3}\|.{5}\|\s*(\d+)?\|.{4}\|.{6}\|.{6}\|.{5}\|.{3}\|.{5}\|.{3}\|.{8}\|\s*(-?\d+)?\|$",
        ]),
        ("L1_PDSCH_Dc_Rslt",vec![
            r"^\|.{3}\|.{9}\|\s*(\d)\|\s*(\d+)\|\s*(\d+)\|.{9}\|.{8}\|\s*(\d+)\|.{9}\|\s*(\d+)\|\s*(\w+)\|.{4}\|\s*(\d+)\|.{5}\|.{7}\|.{3}\|.{5}\|.{7}\|.{10}\|.{9}\|.{8}\|$",
        ]),
        ("L1_PDCCH_Dc_Rslt",vec![
            r"^System Frame Number       = (\d+)$",
            r"^Sub-frame Number          = (\d)$",
            r"^   \|.{3}\|.{18}\|.{11}\|.{9}\|.{6}\|\s*(\w+)\|.{14}\|.{7}\|.{8}\|.{7}\|.{5}\|\s*SUCCESS.*?\|.{10}\|.{8}\|.{6}\|.{8}\|.{5}\|.{6}\|$",
        ]),
        ("RRC_OTA_Packet",vec![
            r"^SysFrameNum = (\d+), SubFrameNum = (\d)$",
            r"^Radio Bearer ID = 0, Physical Cell ID = (\d+)$",
            r"^PDU Number = (.+),    Msg Length = (\d+)$",
        ]),
        ("GNSS_Position_Rpt",vec![
            r"^Position Source = (.+)$",
            r"^Latitude = (\d+.\d+) degree$",
            r"^Longitude = (\d+.\d+) degree$",
        ]),
        ("L1_Nhbr_Cll_Ms_Trk",vec![
            r"^System Frame Number            = (\d+)$",
            r"^Sub-frame Number               = (\d+)$",
            r"^EARFCN                         = (\d+)$",
            r"^   \|.{3}\|\s*(\d+)\|.{10}\|.{8}\|.{7}\|.{4}\|.{9}\|.{7}\|.{7}\|.{7}\|.{6}\|.{6}\|.{26}\|$",
        ]),
        ("Nhbr_Ms_Rqst_Rsp",vec![
            r"^   E-ARFCN         = (\d+)$",
            r"^      \|.{3}\|\s*(\d+)\|.{11}\|.{7}\|.{7}\|\s*(-?\d+\.\d+)\|.{7}\|.{7}\|\s*(-?\d+\.\d+)\|.{7}\|.{7}\|\s*(-?\d+\.\d+)\|$",
        ]),
    ].iter().cloned().collect();
    static ref LPV_R_MAP: HashMap<&'static str, Vec<Regex>> = LPV_RS_MAP.iter().map(|(k,v)| (*k, v.iter().map(|r| Regex::new(r).unwrap()).collect::<Vec<Regex>>())).collect();
    static ref LPV_RS: Vec<RegexSet> = LP_BEGIN_K.iter().map(|k| RegexSet::new(LPV_RS_MAP[k].iter()).unwrap()).collect();
    static ref ORDER_LOOKUP: HashMap<&'static str, i64> = [
        ("First",1),
        ("Second",2),
        ("Third",3),
        ("Fourth",4),
        ("Fifth",5),
        ("Sixth",6),
        ("Seventh",7),
        ("Eighth",8),
        ("Ninth",9),
        ("Tenth",10),
    ].iter().cloned().collect();
}

#[derive(Serialize, Clone)]
struct Row {
    #[serde(rename = "E_ARFCN")]
    earcfn: String,
    #[serde(rename = "Num_Cell")]
    num_cell: String,
    #[serde(rename = "Ph_Cell_ID")]
    pci: String,
    #[serde(rename = "Serv_Cell_Indx")]
    serv_cell_idx: String,
    #[serde(rename = "Is_Serv_Cell")]
    is_serv_cell: String,
    #[serde(rename = "SFN")]
    sfn: String,
    #[serde(rename = "Sub_Fr_N")]
    sub_fr_n: String,
    #[serde(rename = "I_RSRP")]
    rsrp: String,
    #[serde(rename = "I_RSRQ")]
    rsrq: String,
    #[serde(rename = "I_RSSI")]
    rssi: String,
    #[serde(rename = "Tmng_Ofs_Rx")]
    tmng_ofs_rx: String,
    #[serde(rename = "PUCCH_Frmt")]
    pucch_frmt: String,
    #[serde(rename = "PUCCH_Tx_Pwr")]
    pucch_tx_pwr: String,
    #[serde(rename = "Ack_Payload_Length")]
    ack_payload_len: String,
    #[serde(rename = "Re-tx_Index_UL")]
    retx_idx_ul: String,
    #[serde(rename = "Re-tx_Index_DL")]
    retx_idx_dl: String,
    #[serde(rename = "PUSCH_Mod_Order")]
    pusch_mod_order: String,
    #[serde(rename = "PUSCH_Tx_Pwr")]
    pusch_tx_pwr: String,
    #[serde(rename = "Mode")]
    mode: String,
    #[serde(rename = "Pdu_size")]
    pdu_size: String,
    #[serde(rename = "SN")]
    sn: String,
    #[serde(rename = "PHICH_Value")]
    phich_value: String,
    #[serde(rename = "PHICH_1_Value")]
    phich_1_value: String,
    #[serde(rename = "MCS_Index")]
    mcs_idx: String,
    #[serde(rename = "NDI")]
    ndi: String,
    #[serde(rename = "Modulation_Type")]
    modulation_type: String,
    #[serde(rename = "HARQ_ID")]
    harq_id: String,
    #[serde(rename = "DL_Grant_Format_Type")]
    dl_grant_frmt_type: String,
    #[serde(rename = "BSR")]
    bsr: String,
    #[serde(rename = "PHR")]
    phr: String,
    #[serde(rename = "DL_TBS")]
    dl_tbs: String,
    #[serde(rename = "Absolute_TA_Val_xTs")]
    abs_ta: String,
    #[serde(rename = "Relative_TA_Val_xTs")]
    rel_ta: String,
    #[serde(rename = "Number_of_Transport_Blks")]
    num_transport_blks: String,
    #[serde(rename = "Code_Block_Size_Plus")]
    code_block_size: String,
    #[serde(rename = "DCI_Format")]
    dci_frmt: String,
    #[serde(rename = "PDU_Number")]
    pdu_num: String,
    #[serde(rename = "Msg_Length")]
    msg_len: String,
    #[serde(rename = "Latitude")]
    lat: String,
    #[serde(rename = "Longitude")]
    lon: String,
    #[serde(rename = "LP_TYPE")]
    lp_type: String,
    #[serde(rename = "LP_TS")]
    lp_ts: String,
    #[serde(rename = "SN_length")]
    sn_len: String,
    #[serde(rename = "Position_Source")]
    pos_src: String,
}

#[inline(always)]
fn nan() -> String {
    return "".to_string()
}

#[inline(always)]
fn nan_row() -> Row {
    return Row {
        earcfn: nan(),
        num_cell: nan(),
        pci: nan(),
        serv_cell_idx: nan(),
        is_serv_cell: nan(),
        sfn: nan(),
        sub_fr_n: nan(),
        rsrp: nan(),
        rsrq: nan(),
        rssi: nan(),
        tmng_ofs_rx: nan(),
        pucch_frmt: nan(),
        pucch_tx_pwr: nan(),
        ack_payload_len: nan(),
        retx_idx_ul: nan(),
        retx_idx_dl: nan(),
        pusch_mod_order: nan(),
        pusch_tx_pwr: nan(),
        mode: nan(),
        pdu_size: nan(),
        sn: nan(),
        phich_value: nan(),
        phich_1_value: nan(),
        mcs_idx: nan(),
        ndi: nan(),
        modulation_type: nan(),
        harq_id: nan(),
        dl_grant_frmt_type: nan(),
        bsr: nan(),
        phr: nan(),
        dl_tbs: nan(),
        abs_ta: nan(),
        rel_ta: nan(),
        num_transport_blks: nan(),
        code_block_size: nan(),
        dci_frmt: nan(),
        pdu_num: nan(),
        msg_len: nan(),
        lat: nan(),
        lon: nan(),
        lp_type: nan(),
        lp_ts: nan(),
        sn_len: nan(),
        pos_src: nan(),
    }
}

#[inline(always)]
fn clear_row(mut row: Row) -> Row {
    row.earcfn.clear();
    row.num_cell.clear();
    row.pci.clear();
    row.serv_cell_idx.clear();
    row.is_serv_cell.clear();
    row.sfn.clear();
    row.sub_fr_n.clear();
    row.rsrp.clear();
    row.rsrq.clear();
    row.rssi.clear();
    row.tmng_ofs_rx.clear();
    row.pucch_frmt.clear();
    row.pucch_tx_pwr.clear();
    row.ack_payload_len.clear();
    row.retx_idx_ul.clear();
    row.retx_idx_dl.clear();
    row.pusch_mod_order.clear();
    row.pusch_tx_pwr.clear();
    row.mode.clear();
    row.pdu_size.clear();
    row.sn.clear();
    row.phich_value.clear();
    row.phich_1_value.clear();
    row.mcs_idx.clear();
    row.ndi.clear();
    row.modulation_type.clear();
    row.harq_id.clear();
    row.dl_grant_frmt_type.clear();
    row.bsr.clear();
    row.phr.clear();
    row.dl_tbs.clear();
    row.abs_ta.clear();
    row.rel_ta.clear();
    row.num_transport_blks.clear();
    row.code_block_size.clear();
    row.dci_frmt.clear();
    row.pdu_num.clear();
    row.msg_len.clear();
    row.lat.clear();
    row.lon.clear();
    row.lp_type.clear();
    row.lp_ts.clear();
    row.sn_len.clear();
    row.pos_src.clear();
    return row
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let infile = args.get(1).expect("Supply path of text file as first argument.").clone();
    let outfile = args.get(2).expect("Supply path of resulting csv file as second argument.").clone();
    let mut file = File::open(infile.clone()).unwrap();
    let len = file.seek(SeekFrom::End(0)).unwrap();
    let mut handles: Vec<JoinHandle<Writer<Vec<u8>>>> = Vec::new();
    let num_threads: u64 = num_cpus::get() as u64;
    let partition: u64 = len / num_threads;
    let wtr = WriterBuilder::new().delimiter(b';').flexible(true).from_path(&outfile).unwrap();
    for i in 1..num_threads {
        let infile = infile.clone();
        let wtr = WriterBuilder::new().delimiter(b';').flexible(true).has_headers(false).from_writer(vec![]);
        let h = thread::spawn(move || parse_file(infile, wtr, partition * i, partition * (i+1), len));
        handles.push(h);
    }
    let mut wtr = parse_file(infile, wtr, 0, partition, len);
    wtr.flush().unwrap();
    let mut outfile = OpenOptions::new().write(true).append(true).open(outfile).unwrap();
    for handle in handles {
        let buf = handle.join().unwrap().into_inner().unwrap();
        outfile.write_all(&buf).unwrap();
    }
}

fn parse_file<T: Write>(infile: String, mut wtr: Writer<T>, start: u64, end: u64, file_end: u64) -> Writer<T> {
    let lp_f: Vec<&dyn Fn(&Captures, &usize, Row, &mut Writer<T>) -> Row> = vec![
        &l1_srv_cll_ms_rsp,
        &l1_srv_cll_ms_rslt,
        &l1_pucch_tx_rpt,
        &l1_pusch_tx_rpt,
        &pdcp_dl_cd_pdu,
        &pdcp_ul_cd_pdu,
        &pdcch_phich_ind_rpt,
        &dci_info_rpt,
        &mac_ul_tb,
        &mac_dl_tb,
        &l1_pdsch_dc_rslt,
        &l1_pdcch_dc_rslt,
        &rrc_ota_packet,
        &gnss_position_rpt,
        &l1_nhbr_cll_ms_trk,
        &nhbr_ms_rqst_rsp,
    ];
    let lp_ts_r = Regex::new(r"^\d\d\d\d \w+\s+\d+\s+\d+:\d+:\d+\.\d\d\d").unwrap();
    
    let file = File::open(infile).unwrap();
    let mut br = BufReader::new(file);
    br.seek(SeekFrom::Start(start)).unwrap();
    br.read_until(NEW_LINE, &mut vec![]).unwrap(); // Make sure it starts reading on a new line
    #[allow(unused_assignments)]
    let mut matches = LP_BEGIN_RS.matches("".as_bytes());
    let mut lp_k = "";
    let mut lpv_rs = &RegexSet::new([].iter().cloned().collect::<Vec<String>>()).unwrap();
    let mut lp_ts = String::new();
    let mut matches_vec: Vec<usize> = vec![0];
    let mut l_i: &usize = matches_vec.first().unwrap();
    let mut prev_row = nan_row();
    let mut line_begin_pos = br.seek(SeekFrom::Current(0)).unwrap();
    let mut buf: Vec<u8> = Vec::new();
    let mut len = br.read_until(NEW_LINE, &mut buf).unwrap() - 2;
    let mut line_end_pos = line_begin_pos + len as u64 + 2;

    loop {
        matches = LP_BEGIN_RS.matches(&buf[..len]);
        let v_matches = lpv_rs.matches(&buf[..len]);
        if v_matches.matched_any() {
            let v_matches_vec: Vec<_> = v_matches.into_iter().collect();
            let r_i = v_matches_vec.first().unwrap();
            let lpv_r = &LPV_R_MAP[lp_k][*r_i];
            let captures = lpv_r.captures(&buf[..len]).unwrap();
            prev_row.lp_type = lp_k.to_string();
            unsafe { prev_row.lp_ts = String::from_utf8_unchecked(lp_ts.bytes().collect()); }
            prev_row = lp_f[*l_i](&captures, r_i, prev_row, &mut wtr)
        } else if matches.matched_any() {
            if line_begin_pos >= end  { return wtr }
            matches_vec = matches.iter().collect();
            l_i = matches_vec.first().unwrap();
            lp_k = LP_BEGIN_K[*l_i];
            lpv_rs = &LPV_RS[*l_i];
            lp_ts = string(&lp_ts_r.captures(&buf[..len]).unwrap()[0]);
            prev_row = clear_row(prev_row);
        } else if line_end_pos >= file_end { return wtr }
        line_begin_pos = line_end_pos;
        buf.clear();
        len = br.read_until(NEW_LINE, &mut buf).unwrap() - 2;
        line_end_pos += len as u64 + 2;
    }
}

#[inline(always)]
fn string(bytes: &[u8]) -> String {
    unsafe { return String::from_utf8_unchecked(bytes.to_vec()) }
}

#[inline(always)]
fn order_to_int(order: &str) -> String {
    if ORDER_LOOKUP.contains_key(order) {
        return ORDER_LOOKUP[order].to_string()
    }
    let mut order = String::from(order);
    order.pop();
    order.pop();
    return order;
}

#[inline(always)]
fn l1_srv_cll_ms_rsp<T: Write>(captures: &Captures, r_i: &usize, mut prev_row: Row, wtr: &mut Writer<T>) -> Row {
    match r_i {
        0 => prev_row.earcfn = string(&&captures[1]),
        1 => prev_row.num_cell = string(&captures[1]),
        2 => prev_row.pci = string(&captures[1]),
        3 => prev_row.serv_cell_idx = string(&captures[1]),
        4 => prev_row.is_serv_cell = string(&captures[1]),
        5 => prev_row.sfn = string(&captures[1]),
        6 => prev_row.sub_fr_n = string(&captures[1]),
        7 => prev_row.rsrp = string(&captures[1]),
        8 => prev_row.rsrq = string(&captures[1]),
        9 => {
            prev_row.rssi = string(&captures[1]);
            wtr.serialize(&prev_row).unwrap();
            return clear_row(prev_row)
        },
        _ => ()
    };
    return prev_row
}
#[inline(always)]
fn l1_srv_cll_ms_rslt<T: Write>(captures: &Captures, r_i: &usize, mut prev_row: Row, wtr: &mut Writer<T>) -> Row {
    match r_i {
        0 => prev_row.sfn = string(&captures[1]),
        1 => prev_row.sub_fr_n = string(&captures[1]),
        2 => prev_row.pci = string(&captures[1]),
        3 => {
            let tmp_tmgofsrx1 = string(&captures[1]);
            let tmp_tmgofsrx2 = string(&captures[2]);
            let tmp_tmgofsrx = tmp_tmgofsrx1+"."+&tmp_tmgofsrx2;
            prev_row.tmng_ofs_rx = tmp_tmgofsrx;
            wtr.serialize(&prev_row).unwrap();
            return clear_row(prev_row)
        },
        _ => ()
    };
    return prev_row
}
#[inline(always)]
fn l1_pucch_tx_rpt<T: Write>(captures: &Captures, r_i: &usize, mut prev_row: Row, wtr: &mut Writer<T>) -> Row {
    match r_i {
        0 => prev_row.pci = string(&captures[1]),
        1 => {
            prev_row.sfn = match string(&captures[1]) {
                x if x == "" => "0".to_string(),
                num => num
            };
            prev_row.sub_fr_n = string(&captures[2]);
            prev_row.pucch_frmt = string(&captures[3]);
            prev_row.pucch_tx_pwr = string(&captures[4]);
            prev_row.ack_payload_len = string(&captures[5]);
            wtr.serialize(&prev_row).unwrap();
        },
        _ => ()
    };
    return prev_row
}
#[inline(always)]
fn l1_pusch_tx_rpt<T: Write>(captures: &Captures, r_i: &usize, mut prev_row: Row, wtr: &mut Writer<T>) -> Row {
    match r_i {
        0 => prev_row.pci = string(&captures[1]),
        1 => {
            prev_row.sfn = match string(&captures[1]) {
                x if x == "" => "0".to_string(),
                num => num
            };
            prev_row.sub_fr_n = string(&captures[2]);
            prev_row.retx_idx_ul = order_to_int(string(&captures[3]).as_str());
            prev_row.pusch_mod_order = string(&captures[5]);
            prev_row.pusch_tx_pwr = string(&captures[7]);
            wtr.serialize(&prev_row).unwrap();
        },
        _ => ()
    };
    return prev_row
}
#[inline(always)]
fn pdcp_dl_cd_pdu<T: Write>(captures: &Captures, r_i: &usize, mut prev_row: Row, wtr: &mut Writer<T>) -> Row {
    match r_i {
        0 => {
            prev_row.sfn = string(&captures[4]);
            prev_row.sub_fr_n = string(&captures[5]);
            prev_row.mode = string(&captures[1]);
            prev_row.sn_len = string(&captures[2]);
            prev_row.pdu_size = string(&captures[3]);
            prev_row.sn = string(&captures[6]);
            wtr.serialize(&prev_row).unwrap();
            return clear_row(prev_row)
        },
        _ => ()
    };
    return prev_row
}
#[inline(always)]
fn pdcp_ul_cd_pdu<T: Write>(captures: &Captures, r_i: &usize, mut prev_row: Row, wtr: &mut Writer<T>) -> Row {
    match r_i {
        0 => {
            prev_row.sfn = string(&captures[4]);
            prev_row.sub_fr_n = string(&captures[5]);
            prev_row.mode = string(&captures[1]);
            prev_row.sn_len = string(&captures[2]);
            prev_row.pdu_size = string(&captures[3]);
            prev_row.sn = string(&captures[6]);
            wtr.serialize(&prev_row).unwrap();
            return clear_row(prev_row)
        },
        _ => ()
    };
    return prev_row
}
#[inline(always)]
fn pdcch_phich_ind_rpt<T: Write>(captures: &Captures, r_i: &usize, mut prev_row: Row, wtr: &mut Writer<T>) -> Row {
    match r_i {
        0 => {
            prev_row.sfn = string(&captures[1]);
            prev_row.sub_fr_n = string(&captures[2]);
            prev_row.phich_value = string(&captures[4]);
            prev_row.phich_1_value = string(&captures[5]);
            wtr.serialize(&prev_row).unwrap();
            return clear_row(prev_row)
        },
        _ => ()
    };
    return prev_row
}
#[inline(always)]
fn dci_info_rpt<T: Write>(captures: &Captures, r_i: &usize, mut prev_row: Row, wtr: &mut Writer<T>) -> Row {
    match r_i {
        0 => {
            prev_row.sfn = string(&captures[1]);
            prev_row.sub_fr_n = string(&captures[2]);
            prev_row.mcs_idx = string(&captures[3]);
            prev_row.ndi = string(&captures[4]);
            prev_row.modulation_type = string(&captures[5]);
            prev_row.harq_id = string(&captures[6]);
            if captures.get(7).is_some() {
                prev_row.dl_grant_frmt_type = string(&captures[8]);
            }
            wtr.serialize(&prev_row).unwrap();
            return clear_row(prev_row)
        },
        1 => {
            prev_row.sfn = string(&captures[1]);
            prev_row.sub_fr_n = string(&captures[2]);
            prev_row.dl_grant_frmt_type = string(&captures[3]);
            wtr.serialize(&prev_row).unwrap();
            return clear_row(prev_row)
        },
        _ => ()
    };
    return prev_row
}
#[inline(always)]
fn mac_ul_tb<T: Write>(captures: &Captures, r_i: &usize, mut prev_row: Row, wtr: &mut Writer<T>) -> Row {
    match r_i {
        0 => {
            prev_row.sfn = string(&captures[3]);
            prev_row.sub_fr_n = string(&captures[2]);
            prev_row.harq_id = string(&captures[1]);
            prev_row.bsr = string(&captures[4]);
            wtr.serialize(&prev_row).unwrap();
            prev_row.bsr.clear();
        },
        1 => {
            prev_row.phr = string(&captures[1]);
            wtr.serialize(&prev_row).unwrap();
            prev_row.phr.clear()
        },
        _ => ()
    };
    return prev_row
}
#[inline(always)]
fn mac_dl_tb<T: Write>(captures: &Captures, r_i: &usize, mut prev_row: Row, wtr: &mut Writer<T>) -> Row {
    match r_i {
        0 => {
            prev_row.sfn = string(&captures[2]);
            prev_row.sub_fr_n = string(&captures[1]);
            prev_row.harq_id = string(&captures[3]);
            prev_row.dl_tbs = string(&captures[4]);
            if let Some(tmp_abs_ta) = captures.get(5) {
                prev_row.abs_ta = string(&tmp_abs_ta.as_bytes());
            }
            if let Some(tmp_rel_ta) = captures.get(6) {
                prev_row.rel_ta = string(&tmp_rel_ta.as_bytes());
            }
            wtr.serialize(&prev_row).unwrap();
            return clear_row(prev_row)
        },
        _ => ()
    };
    return prev_row
}
#[inline(always)]
fn l1_pdsch_dc_rslt<T: Write>(captures: &Captures, r_i: &usize, mut prev_row: Row, wtr: &mut Writer<T>) -> Row {
    match r_i {
        0 => {
            prev_row.sfn = string(&captures[2]);
            prev_row.sub_fr_n = string(&captures[1]);
            prev_row.harq_id = string(&captures[3]);
            prev_row.num_transport_blks = string(&captures[4]);
            prev_row.ndi = string(&captures[5]);
            prev_row.retx_idx_dl = order_to_int(string(&captures[6]).as_str());
            prev_row.code_block_size = string(&captures[7]);
            wtr.serialize(&prev_row).unwrap();
            return clear_row(prev_row)
        },
        _ => ()
    };
    return prev_row
}
#[inline(always)]
fn l1_pdcch_dc_rslt<T: Write>(captures: &Captures, r_i: &usize, mut prev_row: Row, wtr: &mut Writer<T>) -> Row {
    match r_i {
        0 => prev_row.sfn = string(&captures[1]),
        1 => prev_row.sub_fr_n = string(&captures[1]),
        2 => {
            prev_row.dci_frmt = string(&captures[1]);
            wtr.serialize(&prev_row).unwrap();
        },
        _ => ()
    };
    return prev_row
}
#[inline(always)]
fn rrc_ota_packet<T: Write>(captures: &Captures, r_i: &usize, mut prev_row: Row, wtr: &mut Writer<T>) -> Row {
    match r_i {
        0 => {
            prev_row.sfn = string(&captures[1]);
            prev_row.sub_fr_n = string(&captures[2]);
        },
        1 => prev_row.pci = string(&captures[1]),
        2 => {
            prev_row.pdu_num = string(&captures[1]);
            prev_row.msg_len = string(&captures[2]);
            wtr.serialize(&prev_row).unwrap();
            return clear_row(prev_row)
        },
        _ => ()
    };
    return prev_row
}
#[inline(always)]
fn gnss_position_rpt<T: Write>(captures: &Captures, r_i: &usize, mut prev_row: Row, wtr: &mut Writer<T>) -> Row {
    match r_i {
        0 => prev_row.pos_src = string(&captures[1]),
        1 => prev_row.lat = string(&captures[1]),
        2 => {
            prev_row.lon = string(&captures[1]);
            wtr.serialize(&prev_row).unwrap();
            return clear_row(prev_row)
        },
        _ => ()
    };
    return prev_row
}
#[inline(always)]
fn l1_nhbr_cll_ms_trk<T: Write>(captures: &Captures, r_i: &usize, mut prev_row: Row, wtr: &mut Writer<T>) -> Row {
    match r_i {
        0 => prev_row.sfn = string(&captures[1]),
        1 => prev_row.sub_fr_n = string(&captures[1]),
        2 => prev_row.earcfn = string(&captures[1]),
        3 => {
            prev_row.pci = string(&captures[1]);
            wtr.serialize(&prev_row).unwrap();
        },
        _ => ()
    };
    return prev_row
}
#[inline(always)]
fn nhbr_ms_rqst_rsp<T: Write>(captures: &Captures, r_i: &usize, mut prev_row: Row, wtr: &mut Writer<T>) -> Row {
    match r_i {
        0 => prev_row.earcfn = string(&captures[1]),
        1 => {
            prev_row.pci = string(&captures[1]);
            prev_row.rsrp = string(&captures[2]);
            prev_row.rsrq = string(&captures[3]);
            prev_row.rssi = string(&captures[4]);
            wtr.serialize(&prev_row).unwrap();
        },
        _ => ()
    };
    return prev_row
}

